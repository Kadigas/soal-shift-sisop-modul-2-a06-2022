=======
# LAPORAN RESMI SOAL SHIFT MODUL 2

# Kelompok A06

## Anggota Kelompok

<ul>
    <li>Hilmi Zharfan Rachmadi (5025201268)</li>
    <li>Andhika Ditya Bagaskara D. (5025201096)</li>
    <li>Muhammad Ismail (5025201223)</li>
</ul> 

## Soal no. 1
Pada soal nomor 1, pertama-tama kita diminta untuk mendownload file zip dari link 

https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view 

https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view

```Fungsi dl_gdrive untuk mendownload file dari link Google Drive```
```bash
void dl_gdrive(char link[], char fname[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"wget", "--no-check-certificate", link, "-O", fname, NULL};
    execv("/bin/wget", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}
```

```Pemanggilan fungsi dl_gdrive di dalam fungsi dl_parse```
```bash
void dl_parse()
{
  char c_link[] = "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
 char c_fname[] = "Anggap_ini_database_characters.zip";

 char w_link[] = "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
 char w_fname[] = "Anggap_ini_database_weapon.zip";

 dl_gdrive(c_link, c_fname);
 dl_gdrive(w_link, w_fname);
```
Kemudian kita diminta untuk meng-unzip kedua file tersebut

```Fungsi unzip```
```bash
void unzip(char pants[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"unzip", pants, NULL};
    execv("/bin/unzip", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}
```

```Pemanggilan fungsi unzip dalam fungsi dl_parse```
```bash
unzip(c_fname);
unzip(w_fname);
```
Setelah itu diminta untuk membuat folder "gacha_gacha" sebagai working directory. Untuk membuat folder digunakan fungsi mk_folder
sebagai berikut

```bash
void mk_folder(char str[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "-p", str, NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}
```

```Pemanggilan fungsi mk_folder dalam dl_parse untuk membuat folder "gacha_gacha"```
```bash
mk_folder("gacha_gacha");
```
Selanjutnya, akan dilakukan gacha dengan ketentuan:
- Jika jumlah_gacha genap akan dilakukan gacha item weapons
- Jika jumlah_gacha ganjil akan dilakukan gacha item characters
- Jika jumlah_gacha mod 10 akan akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. 
- Jika jumlah_gacha mod 90 maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.

```Implementasi ketentuan-ketentuan dengan kondisional if else di fungsi dl_parse```
```bash
            if(jumlah_gacha % 2==0)
            {
                char w_dirname[500];
                get_wdir(w_dirname);
                sleep(1);
                parse(w_dirname, jumlah_gacha, primogems, txtname);
            }
            else if(jumlah_gacha % 2!=0)
            {
                char c_dirname[500];
                get_cdir(c_dirname);
                sleep(1);
                parse(c_dirname, jumlah_gacha, primogems, txtname);
            }
            if(jumlah_gacha%10==0)
            {
                benten+=10;
                get_cfname(benten, txtname);
            }
            if(jumlah_gacha%90==0 || jumlah_gacha==(int)79000/160)
            {
                mv_allfiles("./", fdname);
                be90+=90;
                get_cfdname(be90, fdname);

                if(jumlah_gacha<(int)79000/160)
                    mk_folder(fdname);
            }
```
Hasil dari gacha diminta untuk acak/random. Hal ini diimplementasikan dengan cara:

a. List semua filename dalam folder characters dan weapons lalu masukkan ke dalam array

```bash
int toarray(char files[][MAX], char dirname[])
{
     DIR *d;
    struct dirent *dir;
    int idx=0;
    d = opendir(dirname);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {   
            char *str = dir->d_name;
            if(str[0] != '.')
            {
                strcpy(files[idx], str);
                idx++;
            }
            
            //printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
}
```

b. Ambil indeks random dari array tersebut. Fungsi digunakan untuk mendapatkan path menuju random file json di folder characters dan weapons

```bash
void get_wdir(char str[])
{
    int widx_count=countfiles("./weapons");
    char wfiles[widx_count][MAX];
    toarray(wfiles, "./weapons");
    sprintf(str, "./weapons/%s", wfiles[rand() % widx_count]);
}

void get_cdir(char str[])
{
    int cidx_count=countfiles("./characters");
    char cfiles[cidx_count][MAX];
    char c_dirname[500];
    toarray(cfiles, "./characters");
    sprintf(str, "./characters/%s", cfiles[rand() % cidx_count]);
}
```
c. Setelah mendapat path menuju file random, file tersebut akan diparsing

Merujuk pada 1b dan 1c, hasil gacha akan di-outputkan ke dalam file .txt dengan format nama  {Hh:Mm:Ss}_gacha_{jumlah-gacha}.
Tiap-tiap file .txt nantinya akan berisi 10 hasil gacha. 
Fungsi-fungsi di bawah digunakan untuk mendapatkan string nama file .txt tersebut. 

```bash
void get_time(char str[])
{
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(str, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_sec);
  
}

void get_cfname(int n, char str[])
{
  char time[100];
  get_time(time);
  sprintf(str, "%s_gacha_%d.txt", time, n);
  
}
```
Selain itu, 10 file txt akan dimasukkan ke dalam folder dengan format nama total_gacha_{jumlah-gacha}.
Berikut fungsi untuk mendapatkan string sesuai format nama folder tersebut

```bash
void get_cfdname(int n, char str[])
{
  sprintf(str, "./gacha_gacha/total_gacha_%d", n);
  
}
```
Kemudian, di akhir 1c, disebutkan bahwa untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.
Hal ini diimplementasikan dengan menambahkan ```sleep(1)``` pada bagian kondisional-kondisional gacha, sebagaimana dapat dilihat di atas. 

Pada 1d, disebutkan tentang primogems. Di awal, nilai primogems didefinisikan sebanyak 79000.
Tiap gacha mempunyai cost 160. 
Program akan selalu melakukan gacha hingga primogems habis.

Setiap gacha, akan diambil property name dan rarity dari item weapon/character.
Hasil gacha akan outputkan dengan format  {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}.

Berikut keseluruhan implementasi sistem gacha sesuai dengan ketentuan-ketentuan di atas

```Fungsi untuk parsing file json, mengambil property name dan rarity, kemudian di-output sesuai format```
```bash
int parse(char fname[], int jumlah_gacha, int primogems, char txt[])
{
    FILE *fp;
	char buffer[5000];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

	size_t i;	

	fp = fopen(fname,"r");
	fread(buffer, 5000, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char tipe_item[50];
    if(jumlah_gacha % 2==0)
        strcpy(tipe_item, "weapons");
    else if(jumlah_gacha % 2!=0)
        strcpy(tipe_item, "characters");

    FILE * fpter;

    fpter = fopen(txt, "a+");

    fprintf(fpter, "%d_%s_%s_%s_%d\n", jumlah_gacha, tipe_item, json_object_get_string(rarity), json_object_get_string(name), primogems);
    
    fclose(fpter);
}
```

```Fungsi yang mengimplementasikan kondisional gacha beserta primogems```
```bash
void dl_parse()
{
  char c_link[] = "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
 char c_fname[] = "Anggap_ini_database_characters.zip";

 char w_link[] = "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
 char w_fname[] = "Anggap_ini_database_weapon.zip";

 dl_gdrive(c_link, c_fname);
 dl_gdrive(w_link, w_fname);
 unzip(c_fname);
 unzip(w_fname);
  mk_folder("gacha_gacha");


    int jumlah_gacha=0;
    int benten=10;
    int be90=90;
    int primogems = 79000;


    char txtname[255];
    get_cfname(benten, txtname);

    char fdname[255];
    get_cfdname(be90, fdname);
    mk_folder(fdname);

    while(primogems>0)
    {
        primogems -= 160;
        if(primogems>0)
        {
            jumlah_gacha++;

            if(jumlah_gacha % 2==0)
            {
                char w_dirname[500];
                get_wdir(w_dirname);
                sleep(1);
                parse(w_dirname, jumlah_gacha, primogems, txtname);
            }
            else if(jumlah_gacha % 2!=0)
            {
                char c_dirname[500];
                get_cdir(c_dirname);
                sleep(1);
                parse(c_dirname, jumlah_gacha, primogems, txtname);
            }
            if(jumlah_gacha%10==0)
            {
                benten+=10;
                get_cfname(benten, txtname);
            }
            if(jumlah_gacha%90==0 || jumlah_gacha==(int)79000/160)
            {
                mv_allfiles("./", fdname);
                be90+=90;
                get_cfdname(be90, fdname);

                if(jumlah_gacha<(int)79000/160)
                    mk_folder(fdname);
            }
        }
    }
}
```
Setelah gacha selesai, pada 1e disebutkan bahwa semua isi di folder gacha_gacha akan di zip dengan nama
not_safe_for_wibu dengan dipassword "satuduatiga"

Sebelum itu, folder yang ada di dalam folder gacha_gacha akan dikeluarkan terlebih dahulu menggunakan fungsi mv_fdir

```bash
void mv_fdir(char dirname[], char fdname[])
{
    DIR *d;
    struct dirent *dir;
    d = opendir(dirname);
    char path[MAX];
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            {
                strcpy(path, "");
                sprintf(path, "%s%s", dirname, dir->d_name);
                mv_folder(path, fdname);
                //printf("PATH: %s\n", path);
                //printf("fdname: %s\n\n", fdname);
            }
                
        }
        closedir(d);
    }
}
```

Kemudian baru di-zip

```bash
void zip_all()
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "total_gacha_90", "total_gacha_180", "total_gacha_270", "total_gacha_360", "total_gacha_450", "total_gacha_540", NULL};
    execv("/bin/zip", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}
```

Setelah dizip semua folder akan di delete sehingga hanya menyisakan file .zip. Digunakan fungsi snap_thanos

```bash
void snap_thanos()
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"rm", "-r", "gacha_gacha", "total_gacha_90", "total_gacha_180", "total_gacha_270", "total_gacha_360", "total_gacha_450", "total_gacha_540", "weapons", "characters", NULL};
    execv("/bin/rm", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}
```

Keseluruhan alur menge-zip dan menghapus folder ada dalam fungsi berikut

```bash
void zip_snap()
{
  mv_fdir("gacha_gacha/", "./");
    zip_all();
    snap_thanos();
}
```

Fungsi mainnya sebagai berikut

```bash
int main()
{
      dl_parse();
      zip_snap();
}
```

>Output:

<img src="./screenshot/soal1/01.png" alt="alter message">
<img src="./screenshot/soal1/02.png" alt="alter message">
<img src="./screenshot/soal1/03.png" alt="alter message">
<img src="./screenshot/soal1/06.png" alt="alter message">
<img src="./screenshot/soal1/07.png" alt="alter message">

>Kendala

Yang bagian anniversary itu rencananya diimplementasikan pakai daemon, tapi daemonnya belum bisa.
Sempat dicoba seperti ini di main function

```bash
int main()
{
  srand(time(0));
    pid_t pid, sid;        // Variabel untuk menyimpan PID
    struct tm* local;
    time_t t;

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    /* (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    /* (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        t = time(NULL);
        local = localtime(&t);
        if(local->tm_mday == 30 &&
           local->tm_mon == 2 &&
           local->tm_hour == 4 &&
           local->tm_min == 44) dl_parse();
        else if(local->tm_mday == 30 &&
                local->tm_mon == 2 &&
                local->tm_hour == 7 &&
                local->tm_min == 44) zip_snap();
        sleep(30);
    }
    

    return 0;

      
      
}
```
Hasilnya cuma bisa jalan sampai download zip dan buat folder gacha_gacha.
Untuk unzip dan seterusnya tidak bisa.

<img src="./screenshot/soal1/05.png" alt="alter message">

Selain itu, kenyataannya, perbedaan penamaan waktu outputnya sebesar 10 second, bukan 1 second. 

## Soal no. 2

Pada Soal nomor 2, kita diberikan sebuah folder zip bernama ```drakor.zip``` yang nantinya akan di-unzip ke path ```home/[user]/shift2/drakor.
Untuk meeng-unzip folder tersebut, kita pertama-tama membuat nested-fork sampe 6 tingkatan terus menggunakan exec di child ke-6 yang memuat command ```mkdir``` untuk membuat folder ```shift2/drakor```.

```bash 

char *user;
int status;
user = getenv("USER");
strcpy(path,"/home/");
strcat(path,user);
strcat(path,"/shift2/drakor/");
.....
child_id6 = fork();
if(child_id6 < 0) exit(EXIT_FAILURE);
else if(child_id6 == 0){
    //jalankan child 6
    //buat directory
    char *argv[] = {"mkdir","-p",path,NULL};
    execv("/bin/mkdir",argv);
}

```

Setelah membuat folder tersebut, kita dapat mengunzip folder drakor.zip ke folder yang sudah dibuat dengan menggunakan exec juga di parent ke-6 yang memuat command ```unzip```.

```bash
else{
    //jalankan parent 6
    //mengunzip drakor.zip
    while(wait(&status)>0);
    char *argv[] = {"unzip","drakor.zip","-d",path,NULL};
    execv("/bin/unzip",argv);
}
```

Setelah mengunzip, kita diperintahkan untuk menghapus folder yang tidak berguna. Sebelum bisa menghapus folder tersebut, kita harus mengosongkan isi di dalamnya. Untuk itu, pertama-tama kita harus membaca folder utama dulu yaitu ```shift2/drakor``` lalu kita masuk dan membaca isi folder yang tidak penting tersebut. Folder yang tidak penting adalah ```Trash```, ```Coding```, dan ```Song```. Kita dapat menjalankan perintah tersebut di parent ke-5.


```Membaca Folder Utama```
```bash
else{
    //jalankan parent 5
    //hapus folder gk penting
    while(wait(&status)>0);
    DIR *dir;
    struct dirent *dirent;
    dir = opendir(path);
    if(dir != NULL){
        while(dirent = readdir(dir)){
            pembanding1 = strcmp(dirent->d_name,".");
            pembanding2 = strcmp(dirent->d_name,"..");
            if(pembanding1 == 0 || pembanding2 == 0)continue;
            if(strcmp(dirent->d_name,"coding")==0 ||
                strcmp(dirent->d_name,"trash")==0 ||
                strcmp(dirent->d_name,"song")==0){
                char junkPath[100];
                strcpy(junkPath,path);
                strcat(junkPath,dirent->d_name);
                DIR *dirJunk;
                struct dirent *direntJunk;
                dirJunk = opendir(junkPath);
                if(dirJunk != NULL){
                    while(direntJunk = readdir(dirJunk)){
                        if(strcmp(direntJunk->d_name,".") == 0|| strcmp(direntJunk->d_name,"..") == 0)continue;
                        char tempJunkPath[100];
                        strcpy(tempJunkPath,junkPath);
                        strcat(tempJunkPath,"/");
                        strcat(tempJunkPath,direntJunk->d_name);
                        remove(tempJunkPath);
                    }
                    closedir(dirJunk);
                    rmdir(junkPath);
                }
                else perror("Tidak bisa membaca folder");
            }
        }
        closedir(dir);
    }
    else perror("Tidak dapat membaca folder");
}
```
```Membaca Folder yang tidak penting, menghapus file-filenya kemudian menghapus folder tersebut```
```bash
if(strcmp(dirent->d_name,"coding")==0 ||
    strcmp(dirent->d_name,"trash")==0 ||
    strcmp(dirent->d_name,"song")==0){
    char junkPath[100];
    strcpy(junkPath,path);
    strcat(junkPath,dirent->d_name);
    DIR *dirJunk;
    struct dirent *direntJunk;
    dirJunk = opendir(junkPath);
    if(dirJunk != NULL){
        while(direntJunk = readdir(dirJunk)){
            if(strcmp(direntJunk->d_name,".") == 0|| strcmp(direntJunk->d_name,"..") == 0)continue;
            char tempJunkPath[100];
            strcpy(tempJunkPath,junkPath);
            strcat(tempJunkPath,"/");
            strcat(tempJunkPath,direntJunk->d_name);
            remove(tempJunkPath);
        }
        closedir(dirJunk);
        rmdir(junkPath);
    }
    else perror("Tidak bisa membaca folder");
}
```

Setelah kita menghapus folder tidak penting tersebut. Kita diperintahkan untuk membuat folder berdasarkan kategori drakor. Di sini, format file drakor yaitu
```[judul_drakor];[tahun_terbit];[kategori]```. Dengan begitu kita dapat megambil kategori tersebut ketika sudah titik koma (```;```) yang ketiga. Untuk membuat folder kategori tersebut, kita pertama-tama membaca folder utama ```shift2/drakor``` terus mengecheck file-filenya. Berikut merupakan kodingannya

```bash
while(wait(&status)>0);
DIR *dir2;
struct dirent *dirent2;
dir2 = opendir(path);
if(dir2 != NULL){
    while(dirent2 = readdir(dir2)){
        pembanding1 = strcmp(dirent2->d_name,".");
        pembanding2 = strcmp(dirent2->d_name,"..");
        if(pembanding1 == 0 || pembanding2 == 0)continue;
        char direntName[100];
        strcpy(direntName,dirent2->d_name);
        char *token = strtok(direntName, ";");
        while(token != NULL){
            char CategoryPath[100];
            strcpy(CategoryPath,path);
            if(strcmp(token,"action.png")==0)strcat(CategoryPath,"/action/");
            else if(strcmp(token,"school.png")==0)strcat(CategoryPath,"/school/");
            else if(strcmp(token,"horror.png")==0)strcat(CategoryPath,"/horror/");
            else if(strcmp(token,"thriller.png")==0)strcat(CategoryPath,"/thriller/");
            else if(strcmp(token,"comedy.png")==0)strcat(CategoryPath,"/comedy/");
            else if(strcmp(token,"romance.png")==0)strcat(CategoryPath,"/romance/");
            else if(strcmp(token,"fantasy.png")==0)strcat(CategoryPath,"/fantasy/");
            pid_t childParent4 = fork();
            if(childParent4 < 0)exit(EXIT_FAILURE);
            else if (childParent4 == 0){
                char *argv[] = {"mkdir","-p",CategoryPath,NULL};
                execv("/bin/mkdir",argv);
            }
            else while(wait(&status)>0);
            token = strtok(NULL,";");
        }                          
    }
}
else perror("Tidak dapat membaca folder");
```

Kita dapat mengecek titik-koma (```;```) lewat fungsi ```strtok()``` dan untuk kasus ini, terdapat 7 kategori drakor yang ada mulai dari action,school,hingga fantasy. Di sini kita menggunakan ```fork()``` lagi ketika sudah mendeteksi adanya kategori drakor supaya bisa membuat folder kategori tersebut. 

Setelah kita berhasil membuat folder kategori, waktunya kita menaruh file-file drakor ke folder yang sesuai dengan kategorinya. Cara pengerjaannya mirip dengan nomor 2, bedanya saat kita membaca folder utama ```shift2/drakor``` kita harus mengskip folder kategorinya supaya program hanya membaca file-file pentingnya saja. Berikut merupakan kodingannya.

```bash
while(wait(&status)>0);
DIR *dir3;
struct dirent *dirent3;
dir3 = opendir(path);
if(dir3 != NULL){
    int index = 0;
    while(dirent3 = readdir(dir3)){
        pembanding1 = strcmp(dirent3->d_name,".");
        pembanding2 = strcmp(dirent3->d_name,"..");
        if(pembanding1 == 0 || pembanding2 == 0)continue;
        else if(strcmp(dirent3->d_name,"action") == 0) continue;
        else if(strcmp(dirent3->d_name,"romance") == 0) continue;
        else if(strcmp(dirent3->d_name,"thriller") == 0) continue;
        else if(strcmp(dirent3->d_name,"school") == 0) continue;
        else if(strcmp(dirent3->d_name,"horror") == 0) continue;
        else if(strcmp(dirent3->d_name,"comedy") == 0) continue;
        else if(strcmp(dirent3->d_name,"fantasy") == 0) continue;

        char direntName[100];
        char tempdirentName[100];
        char CategoryPath[100];
        strcpy(CategoryPath,path);
        strcpy(direntName,dirent3->d_name);
        strcpy(tempdirentName,direntName);
        if(strstr(direntName,"school.png")){
            strcat(CategoryPath,"school/");
        }
        else if(strstr(direntName,"horror.png")){
            strcat(CategoryPath,"horror/");
        }
        else if(strstr(direntName,"comedy.png")){
            strcat(CategoryPath,"comedy/");
        }
        else if(strstr(direntName,"action.png")){
            strcat(CategoryPath,"action/");
        }
        else if(strstr(direntName,"thriller.png")){
            strcat(CategoryPath,"thriller/");
        }
        else if(strstr(direntName,"romance.png")){
            strcat(CategoryPath,"romance/");
        }
        else if(strstr(direntName,"fantasy.png")){
            strcat(CategoryPath,"fantasy/");
        }
        char *token = strtok(direntName, ";");
        while(token != NULL){
            printf("token tanpa index%s\n",token);
            if(index%3 == 0){
                char CategoryPath2[100];
                char PathFiles[100];
                char JudulDrakor[100];
                printf("%s\n",token);
                strcpy(JudulDrakor,token);
                strcat(JudulDrakor,".png");

                strcpy(CategoryPath2,CategoryPath);

                strcpy(PathFiles,path);
                strcat(PathFiles,tempdirentName);

                strcat(CategoryPath2,JudulDrakor);

                pid_t childParent3 = fork();
                if(childParent3 < 0)exit(EXIT_FAILURE);
                else if (childParent3 == 0){
                    char *argv[] = {"mv",PathFiles,CategoryPath2,NULL};
                    execv("/bin/mv",argv);
                }
                else while(wait(&status)>0);
            }
            index++;
            token = strtok(NULL,";");
        }
    }
}
else perror("Tidak dapat membaca folder");            
```

Terdapat logic statement ```if(index%3)``` digunakan untuk mengambil judul drakor. Selain itu kita menjalankan perintah ini pada parent ke-3. Di dalam parent ke-3, kita juga menggunakan ```fork()``` agar dapat memindahkan file-file drakor sesuai dengan kategorinya dengan menggunakan fungsi ```execv()``` dan memuat command ```mv```.

>Kendala

Program pada soal nomor 2c, untuk penamaan filenya ada yang tidak sesuai dengan apa yang diminta karena masih belum tau cara untuk menghiraukan tanda Underscore (```_```), selain itu ada beberapa file yang tidak bisa dipindahkan karena tidak sesuai dengan penamaan. 

Untuk soal 2d dan 2e masih belum sempat dikerjakan karena tidak tahu logicnya yang mana untuk no 2d sendiri tidak tahu cara memisahkan dari satu file menjadi dua file yang berbeda. Untuk 2e membutuhkan penyelesaian 2d agar dapat mencangkup judul,tahun rilis, dan kategori di semua drakor yang ada

>Hasil

<img src="./screenshot/soal2/01.png" alt="alter message">

## Soal no. 3

Pada soal nomor 3, kita diberikan sebuah zip bernama ```animal.zip``` yang nantinya akan diunzip ke path ```home/[user]/modul2```. Untuk melakukan ini, kita akan menggunakan fungsi ```mkdr()``` untuk membuat directory dan ```unzip()```

```bash
void mkdr(char *path) {

  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/usr/bin/mkdir", argv);

  }
  else {
    while ((wait(&status)) > 0);
  }

}

void unzip() {
	int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"unzip", "-j", "animal.zip", "*.jpg", "-d", "/home/kadigas/modul2/", NULL};
		execv("/usr/bin/unzip", argv);
	}
  else {
		((wait(&status)) > 0);
  }

}

```
Fungsi ```mkdr()``` dan ```unzip()``` sendiri dibuat dengan menggunakan ```fork()```, ```execv()```, dan ```wait()```. 

Untuk soal 3a, dengan menggunakan ```mkdr()```, kita akan mengambil argumen untuk formatting fungsi mkdir ke dalam string ```argv``` dengan format ```{"mkdir", "-p", path, NULL}``` lalu akan menggunakan ```execv("/usr/bin/mkdir", argv)``` untuk mengeksekusi ```mkdir```.

Untuk soal 3b, dengan menggunakanUntuk ```unzip()```, kita akan mengambil argumen untuk formatting fungsi mkdir ke dalam string ```argv``` dengan format ```{"unzip", "-j", "animal.zip", "*.jpg", "-d", "/home/kadigas/modul2/", NULL``` lalu akan menggunakan ```execv("/usr/bin/unzip", argv)``` untuk mengeksekusi ```unzip```. ```-j``` pada ```unzip()``` berfungsi agar file yang diunzip akan langsung didump ke dalam path, sehingga tidak perlu lagi membuat folder ```animal``` di dalam path. 

Untuk ```mkdr()``` memerlukan parameter string ```path``` sehingga ketika fungsi dipanggil, kita perlu passing sebuah string, yaitu path yang akan dibuat. Sehingga pada main:

```bash
int main() {
  char path_modul2[50] = "/home/kadigas/modul2/";
  char path_darat[100] = "/home/kadigas/modul2/darat/";
  char path_air[100] = "/home/kadigas/modul2/air/";

  mkdr(path_modul2);
  mkdr(path_darat);

  sleep(3);
  mkdr(path_air);

  unzip();
```
Untuk memudahkan pengerjaan ke depannya, kita akan membuat 3 string, yaitu ```path_modul2``` yang akan menyimpan path ```/home/[user]/modul2/```,  ```path_darat``` yang akan menyimpan path ```/home/[user]/modul2/darat/```, dan ```path_air``` yang akan menyimpan path ```/home/[user]/modul2/air/```

Pada saat membuat directory, kita diminta untuk membuat directory ```modul2/darat```, lalu terjadi selang selama 3 detik, seteleah itu directory ```modul2/air``` akan dibuat. Untuk itu, sebelum kita memanggil ```mkdr(path_air)```, kita akan memanggil fungsi ```sleep(3)``` untuk membuat selang waktu 3 detik. Setelah membuat directory, kita akan memanggil ```unzip()``` untuk mengunzip file yang ada.

Untuk soal 3c, kita diminta untuk memisahkan gambar yang telah diunzip sesuai dengan yang kategori pada masing-masing gambar, yaitu "air" dan "darat". Untuk itu, kita perlu menggunakan directory listing yang nantinya akan memanggil fungsi untuk move file ketika kondisi terpenuhi. Untuk directory listing perlu passing path yang akan dilist. Maka pada main:

```bash
  listFiles(path_modul2);
```
Fungsi ```listFiles()``` akan membaca semua file yang ada di dalam ```path_modul2``` hingga ```NULL``` atau "habis". Fungsi akan mengecek apabila name sebuah file bukan ```.```, ```..```, dan merupakan sebuah ```.jpg```.

```bash
void listFiles(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
          
        if(strstr(dp->d_name, "darat")){
          mvdarat(src);
        }
        else if(strstr(dp->d_name, "air")){
          mvair(src);
        }
        else{
          rmv(src);
        }
      }
    }

    closedir(dir);
}
```
Ketika kondisi pertama terpenuhi, ```basePath``` yaitu ```/home/[user]/modul2/``` akan dicopy ke variable string ```src```, lalu ditambah dengan name dari file sekarang (misalkan dp->d_name = ```bear_darat.jpg```, maka ```src``` akan menjadi ```/home/[user]/modul2/`bear_darat.jpg```). Setelah itu, kita akan mengecek name dari file sekarang, apabila pada substring name berisikan ```darat```, maka kita akan memanggil ```mvdarat()```. Jika pada substring name berisikan ```air```, maka kita akan memanggil ```mvair()```. Untuk memanggil kedua fungsi ini, kita perlu passing path yang akan dimove. Dalam kasus sekarang, kita sudah memiliki path dari file yang sudah kita simpan dalam ```src```. Selain itu, jika dalam name tidak mengandung kedua kata tadi, maka file tersebut harus dihapus. Untuk itu, kita akan memanggil fungsi ```rmv()``` untuk menghapus file tersebut dengan passing path dari file tersebut.

```bash
void mvdarat(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"mv", path, "/home/kadigas/modul2/darat/", NULL};
		execv("/usr/bin/mv", argv);
	}
  else {
		((wait(&status)) > 0);
	}

}

void mvair(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"mv", path, "/home/kadigas/modul2/air/", NULL};
		execv("/usr/bin/mv", argv);
	}
  else {
		((wait(&status)) > 0);
	}


}

```
Pada fungsi ```mvdarat()``` dan ```mvair()``` memiliki fungsi yang serupa. Kedua fungsi ini akan menggunakan ```fork()``` sehingga pada child nya terdapat variable string ```argv``` untuk menyimpan argumen formatting ```mv```. Untuk ```mvdarat()```, isi ```argv``` adalah ```{"mv", path, "/home/kadigas/modul2/darat/", NULL}``` sedangkan untuk ```mvair()```, isi ```argv``` adalah ``` {"mv", path, "/home/kadigas/modul2/air/", NULL}```. Jadi, perbedaan kedua fungsi ini ada pada path destination-nya. Setelah itu, menggunakan ```execv("/usr/bin/mv", argv)``` untuk mengeksekusi ```mv``` sehingga path yang diambil akan dimove ke path destination.

```bash
void rmv(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"rm", path, NULL};
		execv("/usr/bin/rm", argv);
	}
  else {
		((wait(&status)) > 0);
	}

}
```
Fungsi ```rmv()``` juga aka menggunakan ```fork()``` sehingga pada child nya terdapat variable string ```argv``` untuk menyimpan argumen formatting ```rmv```. Isi dari ```argv``` sendiri adalah ```{"rm", path, NULL}``` dan setelah itu menggunakan ```execv("/usr/bin/rm", argv)``` untuk mengeksekusi ```rmv``` sehingga file dengan path yang diambil akan dihapus.

Untuk soal 3d, kita perlu menghapus file dengan kategori burung yang ditandai dengan adanya substring "bird". Untuk itu, dengan menggunakan cara yang serupa, yaitu membaca seluruh path file yang ada dalam sebuah directory, kita dapat memanggil fungsi ```rmv()``` ketika file dengan substring "bird" ditemukan. Untuk melakukan ini, directory yang akan dipassing adalah ```/home/kadigas/modul2/darat/``` yang disimpan dalam ```path_darat```. Maka dalam main:

```bash
  listFilesBird(path_darat);
```
Fungsi ```listFilesBird()``` sendiri berisi:

```bash
void listFilesBird(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, "bird") && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        rmv(src);
      }
    }

    closedir(dir);
}
```
Dalam ```listFilesBird()```, program akan membaca dan mengecek kondisi apabila file name bukan ```.```, ```..```, memiliki substring "bird", dan merupakan ```.jpg```. Bila kondisi telah memenuhi, string ```basePath```, yang adalah path yang disimpan ```path_darat```, akan dicopy ke dalam string ```src``` dan ```src``` kemudian akan ditambahkan dengan file name sekarang (cara yang sama seperti sebelumnya). Setelah itu, kita memanggil fungsi ```rmv()``` dengan passing ```src``` untuk meremove file dengan path yang disimpan oleh ```src```.

Untuk soal 3e, kita perlu membuat file ```.txt``` dengan nama ```list.txt``` untuk membuat list nama file yang ada pada folder ```air```. Isi dari list pun memiliki format ```UID_[UID file permission]_Nama File.[jpg/png]``` di mana ```UID``` adalah user dari file tersebut dan ```[UID file permission]``` adalah permission dari file tersebut. Untuk itu, dengan cara yang sama, yaitu membaca seluruh path yang ada pada directory ```/home/kadigas/modul2/air/```, kita akan membuat file dan mendaftar semua file yang ada. Untuk pemanggilan di main:

```bash
  listFilesAir(path_air);

  return 0;
}
```
Dengan menggunakan ```listFilesAir()``` dan passing directory yang tersimpan dalam ```path_air```:

```bash
void listFilesAir(char *basePath)
{
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul2/air/list.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        strcpy(list, "");
        permission(src, list);
        strcat(list, dp->d_name);
        fprintf(fptr, "%s\n", list);
      }
    }

    fclose(fptr);
    closedir(dir);
}
```
Kita akan membuat ```list.txt``` dengan menggunakan ```FILE *fptr``` dengan mode ```w+``` untuk membaca dan menulis pada file ```list.txt```. Ketika program membaca file, program akan mengecek kondisi apabila nama file bukan ```.```, ```..```, dan merupakan sebuah ```.jpg```, maka string ```basePath``` yang berisi path dari directory akan dicopy ke string ```src``` yang kemudian ditambahkan file name yang sekarang dibaca. Setelah itu, inisialisasi string ```list``` dilakukan untuk menginisialisasi dan mereset ulang ```list``` untuk iterasi selanjutnya. Setelah itu, denga menggunakan ```permission()``` kita akan membuat formatting dan menyimpannya di dalam ```list```. ```permission()``` nantinya akan menyimpan ```UID_[UID file permission]_``` ke dalam ```list```. Setelah itu, ```list``` akan ditambahkan dengan file name sekarang, kemudian diprint ke dalam file ```list.txt```.

Untuk fungsi ```permission()```, kita akan passing string ```src``` yang telah menyimpan path directory serta file name dan string ```list``` yang telah diinisialisasi:

```bash
void permission(char *path, char *list){
    struct stat fs;
    int r;

    r = stat(path,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(fs.st_uid);
    struct group  *gr = getgrgid(fs.st_gid);

    if (pw != 0) strcat(list, pw->pw_name);
    strcat(list, "_");

    if( fs.st_mode & S_IRUSR )
        strcat(list, "r");
    if( fs.st_mode & S_IWUSR )
        strcat(list, "w");
    if( fs.st_mode & S_IXUSR )
        strcat(list, "x");
    strcat(list, "_");

}
```
Dengan menggunakan library ```sys/stat.h``` dan ```pwd.h```, kita akan mengambil mendeklarasi ```stat fs```. integer ```r``` berfungsi untuk mengecek apabila file dapat dibaca (menggunakan ```stat(path, &fs)```) agar jika tidak maka program akan langsung diexit. Apabila file dapat dibaca, maka program akan mengambil UID dengan ```getpwuid(fs.st_uid)``` dan disimpan dalam pointer ```*pw```. Jika ```pw``` tidak sama dengan 0, maka kita akan menambahkan ```pw->pw_name``` ke dalam ```list``` untuk mendapatkan username dari UID yang memiliki file tersebut. Setelah itu, tambahkan ```_``` untuk formatting, dan mulai pengecekan permission. Apabila kondisi ```fs.st_mode & S_IRUSR``` memenuhi, maka file memiliki permission ```read```, sehingga tambahkan ```r``` pada ```list```. Apabila kondisi ```fs.st_mode & S_IWUSR``` memenuhi, maka file memiliki permission ```write```, sehingga tambahkan ```w``` pada ```list```. Apabila kondisi ```fs.st_mode & S_IXUSR``` memenuhi, maka file memiliki permission ```execute```, sehingga tambahkan ```x``` pada ```list```. Apabila semua pengecekan berakhir, tambahkan ```_``` ke dalam ```list```. Karena kita passing ```list``` by reference, maka semua isi ```list``` yang berubah karena fungsi ini akan tetap disimpan.

Setelah menjalankan ```permission()```, program akan melanjutkan ```listFilesAir()``` sebelumnya dan bekerja seperti yang telah dijelaskan sebelumnya.

Screenshots:
3a
<img src="./screenshot/soal3/01.png" alt="alter message">
3b
<img src="./screenshot/soal3/02.png" alt="alter message">
3c
<img src="./screenshot/soal3/03.png" alt="alter message">
<img src="./screenshot/soal3/04.png" alt="alter message">
3d
<img src="./screenshot/soal3/05.png" alt="alter message">
3e
<img src="./screenshot/soal3/06.png" alt="alter message">
