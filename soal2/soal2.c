#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>

#pragma GCC diagnostic ignored "-Wwrite-strings"
pid_t child_id1,child_id2,child_id3,child_id4,child_id5,child_id6;
char path[500];
char pathCategory[500];
char junkFolder[10];
int pembanding1,pembanding2;

int main(){
    char *user;
    int status;
    user = getenv("USER");
    strcpy(path,"/home/");
    strcat(path,user);
    strcat(path,"/shift2/drakor/");

    child_id1 = fork();
    if(child_id1 < 0) exit(EXIT_FAILURE);
    else if(child_id1 == 0){
        //jalankan child 1
        child_id2 = fork();
        if(child_id2 < 0) exit(EXIT_FAILURE);
        else if(child_id2 == 0){
            //jalankan child 2
            child_id3 = fork();
            if(child_id3 < 0) exit(EXIT_FAILURE);
            else if(child_id3 == 0){
                //jalankan child 3
                child_id4 = fork();
                if(child_id4 < 0) exit(EXIT_FAILURE);
                else if(child_id4 == 0){
                    //jalankan child 4
                    child_id5 = fork();
                    if(child_id5 < 0) exit(EXIT_FAILURE);
                    else if(child_id5 == 0){
                        //jalankan child 5
                        child_id6 = fork();
                        if(child_id6 < 0) exit(EXIT_FAILURE);
                        else if(child_id6 == 0){
                            //jalankan child 6
                            //buat directory
                            char *argv[] = {"mkdir","-p",path,NULL};
                            execv("/bin/mkdir",argv);
                        }
                        else{
                            //jalankan parent 6
                            //mengunzip drakor.zip
                            while(wait(&status)>0);
                            char *argv[] = {"unzip","drakor.zip","-d",path,NULL};
                            execv("/bin/unzip",argv);
                        }
                    }
                    else{
                        //jalankan parent 5
                        //hapus folder gk penting
                        while(wait(&status)>0);
                        DIR *dir;
                        struct dirent *dirent;
                        dir = opendir(path);
                        if(dir != NULL){
                            while(dirent = readdir(dir)){
                                pembanding1 = strcmp(dirent->d_name,".");
                                pembanding2 = strcmp(dirent->d_name,"..");
                                if(pembanding1 == 0 || pembanding2 == 0)continue;
                                if(strcmp(dirent->d_name,"coding")==0 ||
                                   strcmp(dirent->d_name,"trash")==0 ||
                                   strcmp(dirent->d_name,"song")==0){
                                    char junkPath[100];
                                    strcpy(junkPath,path);
                                    strcat(junkPath,dirent->d_name);
                                    DIR *dirJunk;
                                    struct dirent *direntJunk;
                                    dirJunk = opendir(junkPath);
                                    if(dirJunk != NULL){
                                        while(direntJunk = readdir(dirJunk)){
                                            if(strcmp(direntJunk->d_name,".") == 0|| strcmp(direntJunk->d_name,"..") == 0)continue;
                                            char tempJunkPath[100];
                                            strcpy(tempJunkPath,junkPath);
                                            strcat(tempJunkPath,"/");
                                            strcat(tempJunkPath,direntJunk->d_name);
                                            remove(tempJunkPath);
                                        }
                                        closedir(dirJunk);
                                        rmdir(junkPath);
                                    }
                                    else perror("Tidak bisa membaca folder");
                                }
                            }
                            closedir(dir);
                        }
                        else perror("Tidak dapat membaca folder");
                    }
                }
                else{
                    //jalankan parent 4
                    //buat kategori folder
                    while(wait(&status)>0);
                    DIR *dir2;
                    struct dirent *dirent2;
                    dir2 = opendir(path);
                    if(dir2 != NULL){
                        while(dirent2 = readdir(dir2)){
                            pembanding1 = strcmp(dirent2->d_name,".");
                            pembanding2 = strcmp(dirent2->d_name,"..");
                            if(pembanding1 == 0 || pembanding2 == 0)continue;
                            char direntName[100];
                            strcpy(direntName,dirent2->d_name);
                            char *token = strtok(direntName, ";");
                            while(token != NULL){
                                char CategoryPath[100];
                                strcpy(CategoryPath,path);
                                if(strcmp(token,"action.png")==0)strcat(CategoryPath,"/action/");
                                else if(strcmp(token,"school.png")==0)strcat(CategoryPath,"/school/");
                                else if(strcmp(token,"horror.png")==0)strcat(CategoryPath,"/horror/");
                                else if(strcmp(token,"thriller.png")==0)strcat(CategoryPath,"/thriller/");
                                else if(strcmp(token,"comedy.png")==0)strcat(CategoryPath,"/comedy/");
                                else if(strcmp(token,"romance.png")==0)strcat(CategoryPath,"/romance/");
                                else if(strcmp(token,"fantasy.png")==0)strcat(CategoryPath,"/fantasy/");
                                pid_t childParent4 = fork();
                                if(childParent4 < 0)exit(EXIT_FAILURE);
                                else if (childParent4 == 0){
                                    char *argv[] = {"mkdir","-p",CategoryPath,NULL};
                                    execv("/bin/mkdir",argv);
                                }
                                else while(wait(&status)>0);
                                token = strtok(NULL,";");
                            }                          
                        }
                    }
                    else perror("Tidak dapat membaca folder");
                }
            }
            else{
                //jalankan parent 3
                //memindahkan file ke folder kategori
                while(wait(&status)>0);
                DIR *dir3;
                struct dirent *dirent3;
                dir3 = opendir(path);
                if(dir3 != NULL){
                    int index = 0;
                    while(dirent3 = readdir(dir3)){
                        pembanding1 = strcmp(dirent3->d_name,".");
                        pembanding2 = strcmp(dirent3->d_name,"..");
                        if(pembanding1 == 0 || pembanding2 == 0)continue;
                        else if(strcmp(dirent3->d_name,"action") == 0) continue;
                        else if(strcmp(dirent3->d_name,"romance") == 0) continue;
                        else if(strcmp(dirent3->d_name,"thriller") == 0) continue;
                        else if(strcmp(dirent3->d_name,"school") == 0) continue;
                        else if(strcmp(dirent3->d_name,"horror") == 0) continue;
                        else if(strcmp(dirent3->d_name,"comedy") == 0) continue;
                        else if(strcmp(dirent3->d_name,"fantasy") == 0) continue;

                        char direntName[100];
                        char tempdirentName[100];
                        char CategoryPath[100];
                        strcpy(CategoryPath,path);
                        strcpy(direntName,dirent3->d_name);
                        strcpy(tempdirentName,direntName);
                        if(strstr(direntName,"school.png")){
                            strcat(CategoryPath,"school/");
                        }
                        else if(strstr(direntName,"horror.png")){
                            strcat(CategoryPath,"horror/");
                        }
                        else if(strstr(direntName,"comedy.png")){
                            strcat(CategoryPath,"comedy/");
                        }
                        else if(strstr(direntName,"action.png")){
                            strcat(CategoryPath,"action/");
                        }
                        else if(strstr(direntName,"thriller.png")){
                            strcat(CategoryPath,"thriller/");
                        }
                        else if(strstr(direntName,"romance.png")){
                            strcat(CategoryPath,"romance/");
                        }
                        else if(strstr(direntName,"fantasy.png")){
                            strcat(CategoryPath,"fantasy/");
                        }
                        char *token = strtok(direntName, ";");
                        while(token != NULL){
                            if(index%3 == 0){
                                char CategoryPath2[100];
                                char PathFiles[100];
                                char JudulDrakor[100];
                                strcpy(JudulDrakor,token);
                                strcat(JudulDrakor,".png");
                                strcpy(CategoryPath2,CategoryPath);
                                strcpy(PathFiles,path);
                                strcat(PathFiles,tempdirentName);
                                strcat(CategoryPath2,JudulDrakor);

                                pid_t childParent3 = fork();
                                if(childParent3 < 0)exit(EXIT_FAILURE);
                                else if (childParent3 == 0){
                                    char *argv[] = {"mv",PathFiles,CategoryPath2,NULL};
                                    execv("/bin/mv",argv);
                                }
                                else while(wait(&status)>0);
                            }
                            index++;
                            token = strtok(NULL,";");
                        }
                    }
                }
                else perror("Tidak dapat membaca folder");
                
            }
        }
        else{
            //jalankan parent2
            //memindah file yang dobel ke folder lain
            while(wait(&status)>0);
        }
    }
    else{
        //jalankan parent 1
        //buat file data.txt
        while(wait(&status)>0);
    }

}
/*
Referensi
https://stackoverflow.com/questions/31906192/how-to-use-environment-variable-in-a-c-program
https://www.tutorialspoint.com/c_standard_library/c_function_getenv.htm
https://linuxize.com/post/how-to-unzip-files-in-linux/
https://stackoverflow.com/questions/16344347/how-to-find-directory-in-linux-using-c
https://www.tutorialspoint.com/c_standard_library/c_function_remove.htm
https://www.geeksforgeeks.org/using-fork-produce-1-parent-3-child-processes/
https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm
https://java2blog.com/check-if-string-contains-substring-cpp/
https://www.geeksforgeeks.org/how-to-split-a-string-in-cc-python-and-java/
*/