#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

void mkdr(char *path) {

  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", path, NULL};
    execv("/usr/bin/mkdir", argv);

  }
  else {
    while ((wait(&status)) > 0);
  }

}

void unzip() {
	int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"unzip", "-j", "animal.zip", "*.jpg", "-d", "/home/kadigas/modul2/", NULL};
		execv("/usr/bin/unzip", argv);
	}
  else {
		((wait(&status)) > 0);
  }

  //Referensi: https://stackoverflow.com/questions/14767650/c-unzip-returns-cannot-create-extraction-directory diakses pada: 19/03/2022 12:30
}



void mvdarat(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"mv", path, "/home/kadigas/modul2/darat/", NULL};
		execv("/usr/bin/mv", argv);
	}
  else {
		((wait(&status)) > 0);
	}

  //Referensi: https://www.ibm.com/docs/en/aix/7.1?topic=m-mv-command diakses pada: 19/03/2022 14:10
}

void mvair(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"mv", path, "/home/kadigas/modul2/air/", NULL};
		execv("/usr/bin/mv", argv);
	}
  else {
		((wait(&status)) > 0);
	}

  ///Referensi: https://www.ibm.com/docs/en/aix/7.1?topic=m-mv-command diakses pada: 19/03/2022 14:10
}

void rmv(char *path){
  int status;
	pid_t child_id = fork();
  
  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }
	
  if (child_id == 0) {
		char *argv[] = {"rm", path, NULL};
		execv("/usr/bin/rm", argv);
	}
  else {
		((wait(&status)) > 0);
	}

  //Referensi: https://www.ibm.com/docs/en/aix/7.1?topic=r-rm-command  diakses pada: 19/03/2022 14:37
}

void listFiles(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
          
        if(strstr(dp->d_name, "darat")){
          mvdarat(src);
        }
        else if(strstr(dp->d_name, "air")){
          mvair(src);
        }
        else{
          rmv(src);
        }
      }
    }

    closedir(dir);
}

void listFilesBird(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, "bird") && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        rmv(src);
      }
    }

    closedir(dir);
}


void permission(char *path, char *list){
    struct stat fs;
    int r;

    r = stat(path,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(fs.st_uid);
    struct group  *gr = getgrgid(fs.st_gid);

    if (pw != 0) strcat(list, pw->pw_name);
    strcat(list, "_");

    if( fs.st_mode & S_IRUSR )
        strcat(list, "r");
    if( fs.st_mode & S_IWUSR )
        strcat(list, "w");
    if( fs.st_mode & S_IXUSR )
        strcat(list, "x");
    strcat(list, "_");

}

void listFilesAir(char *basePath)
{
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul2/air/list.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        strcpy(list, "");
        permission(src, list);
        strcat(list, dp->d_name);
        fprintf(fptr, "%s\n", list);
      }
    }

    fclose(fptr);
    closedir(dir);
}



int main() {

  char path_modul2[50] = "/home/kadigas/modul2/";
  char path_darat[100] = "/home/kadigas/modul2/darat/";
  char path_air[100] = "/home/kadigas/modul2/air/";


  //membuat folder modul2
  mkdr(path_modul2);
  //membuat folder darat
  mkdr(path_darat);
        
  //membuat folder air setelah 3 sec
  sleep(3);
  mkdr(path_air);

  unzip();
  listFiles(path_modul2);
  listFilesBird(path_darat);
  listFilesAir(path_air);

  return 0;
}