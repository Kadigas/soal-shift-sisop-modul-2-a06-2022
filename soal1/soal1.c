#include<stdio.h>
#include<json-c/json.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#define MAX 500

void get_time(char str[])
{
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(str, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_sec);
  
}

void get_cfname(int n, char str[])
{
  char time[100];
  get_time(time);
  sprintf(str, "%s_gacha_%d.txt", time, n);
  
}

int countfiles(char dirname[])
{
    DIR *d;
    struct dirent *dir;
    int filecount=0;
    d = opendir(dirname);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            filecount++;
            //printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
    return filecount-2;
}

int toarray(char files[][MAX], char dirname[])
{
     DIR *d;
    struct dirent *dir;
    int idx=0;
    d = opendir(dirname);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {   
            char *str = dir->d_name;
            if(str[0] != '.')
            {
                strcpy(files[idx], str);
                idx++;
            }
            
            //printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
}

void get_wdir(char str[])
{
    int widx_count=countfiles("./weapons");
    char wfiles[widx_count][MAX];
    toarray(wfiles, "./weapons");
    sprintf(str, "./weapons/%s", wfiles[rand() % widx_count]);
}

void get_cdir(char str[])
{
    int cidx_count=countfiles("./characters");
    char cfiles[cidx_count][MAX];
    char c_dirname[500];
    toarray(cfiles, "./characters");
    sprintf(str, "./characters/%s", cfiles[rand() % cidx_count]);
}

int parse(char fname[], int jumlah_gacha, int primogems, char txt[])
{
    FILE *fp;
	char buffer[5000];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

	size_t i;	

	fp = fopen(fname,"r");
	fread(buffer, 5000, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char tipe_item[50];
    if(jumlah_gacha % 2==0)
        strcpy(tipe_item, "weapons");
    else if(jumlah_gacha % 2!=0)
        strcpy(tipe_item, "characters");

    FILE * fpter;

    fpter = fopen(txt, "a+");

    fprintf(fpter, "%d_%s_%s_%s_%d\n", jumlah_gacha, tipe_item, json_object_get_string(rarity), json_object_get_string(name), primogems);
    
    fclose(fpter);
}

void get_cfdname(int n, char str[])
{
  sprintf(str, "./gacha_gacha/total_gacha_%d", n);
  
}

void mk_folder(char str[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "-p", str, NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void dl_gdrive(char link[], char fname[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"wget", "--no-check-certificate", link, "-O", fname, NULL};
    execv("/bin/wget", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void unzip(char pants[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"unzip", pants, NULL};
    execv("/bin/unzip", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void mv_folder(char str[], char fdname[])
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mv", str, fdname, NULL};
    execv("/bin/mv", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void mv_allfiles(char dirname[], char fdname[])
{
    DIR *d;
    struct dirent *dir;
    d = opendir(dirname);
    char path[MAX];
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0 && strstr(dir->d_name, ".txt"))
            {
                strcpy(path, "");
                sprintf(path, "%s%s", dirname, dir->d_name);
                mv_folder(path, fdname);
                //printf("PATH: %s\n", path);
                //printf("fdname: %s\n\n", fdname);
            }
                
        }
        closedir(d);
    }
}

void zip_all()
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "total_gacha_90", "total_gacha_180", "total_gacha_270", "total_gacha_360", "total_gacha_450", "total_gacha_540", NULL};
    execv("/bin/zip", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void mv_fdir(char dirname[], char fdname[])
{
    DIR *d;
    struct dirent *dir;
    d = opendir(dirname);
    char path[MAX];
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            {
                strcpy(path, "");
                sprintf(path, "%s%s", dirname, dir->d_name);
                mv_folder(path, fdname);
                //printf("PATH: %s\n", path);
                //printf("fdname: %s\n\n", fdname);
            }
                
        }
        closedir(d);
    }
}

void snap_thanos()
{
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"rm", "-r", "gacha_gacha", "total_gacha_90", "total_gacha_180", "total_gacha_270", "total_gacha_360", "total_gacha_450", "total_gacha_540", "weapons", "characters", NULL};
    execv("/bin/rm", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
  }
}

void dl_parse()
{
  char c_link[] = "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
 char c_fname[] = "Anggap_ini_database_characters.zip";

 char w_link[] = "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
 char w_fname[] = "Anggap_ini_database_weapon.zip";

 dl_gdrive(c_link, c_fname);
 dl_gdrive(w_link, w_fname);
 unzip(c_fname);
 unzip(w_fname);
  mk_folder("gacha_gacha");


    int jumlah_gacha=0;
    int benten=10;
    int be90=90;
    int primogems = 79000;


    char txtname[255];
    get_cfname(benten, txtname);

    char fdname[255];
    get_cfdname(be90, fdname);
    mk_folder(fdname);

    while(primogems>0)
    {
        primogems -= 160;
        if(primogems>0)
        {
            jumlah_gacha++;

            if(jumlah_gacha % 2==0)
            {
                char w_dirname[500];
                get_wdir(w_dirname);
                sleep(1);
                parse(w_dirname, jumlah_gacha, primogems, txtname);
            }
            else if(jumlah_gacha % 2!=0)
            {
                char c_dirname[500];
                get_cdir(c_dirname);
                sleep(1);
                parse(c_dirname, jumlah_gacha, primogems, txtname);
            }
            if(jumlah_gacha%10==0)
            {
                benten+=10;
                get_cfname(benten, txtname);
            }
            if(jumlah_gacha%90==0 || jumlah_gacha==(int)79000/160)
            {
                mv_allfiles("./", fdname);
                be90+=90;
                get_cfdname(be90, fdname);

                if(jumlah_gacha<(int)79000/160)
                    mk_folder(fdname);
            }
        }
    }
}

void zip_snap()
{
  mv_fdir("gacha_gacha/", "./");
    zip_all();
    snap_thanos();
}

int main()
{
      dl_parse();
      zip_snap();
}

/* Reference
 * https://www.geeksforgeeks.org/creating-multiple-process-using-fork/ - 3-18-22 
 * http://www.kuliahkomputer.com/2020/05/cara-download-google-drive-menggunakan.html 3-16-22
 * https://progur.com/2018/12/how-to-parse-json-in-c.html - 3/19/22
 * https://www.youtube.com/watch?v=dQyXuFWylm4 - 3/19/22 
 * https://www.sanfoundry.com/c-program-list-files-directory/ 
 * https://stackoverflow.com/questions/4204666/how-to-list-files-in-a-directory-in-a-c-program
 * https://www.geeksforgeeks.org/generating-random-number-range-c/
 * https://www.geeksforgeeks.org/zip-command-in-linux-with-examples/
 * https://www.earthdatascience.org/courses/intro-to-earth-data-science/open-reproducible-science/bash/bash-commands-to-manage-directories-files/#:~:text=Delete%20a%20Directory%20(%20rm%20%2Dr,%2Dr%20directory%2Dname%20). */